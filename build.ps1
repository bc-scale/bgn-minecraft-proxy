$ErrorActionPreference = "Stop"
# Fixes a silly bug Microsoft never bothered to fix in 5.1 which is slow downloads if you have the progress bar shown. -_-
$ProgressPreference = 'SilentlyContinue'

# This section is less than ideal but we need to get 3 bits of information to do a download of the latest paper build, the papermc.io project does not provide a latest link for stable downloads!
function Get-LatestProxyVersion() {
    $versionResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/velocity" | ConvertFrom-Json
    $buildResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/velocity/versions/$($versionResponse.versions[-1])" | ConvertFrom-Json
    $downloadNameResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/velocity/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])" | ConvertFrom-Json

    if (-Not (Test-Path -Path "./src/proxy.jar")) {
        Write-Output "Downloading Latest Proxy Jar"
        Write-Output "https://papermc.io/api/v2/projects/velocity/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/proxy.jar"
        Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/velocity/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/proxy.jar"
    }
    else {
        if (-Not ($($(Get-FileHash -Path "./src/proxy.jar" -Algorithm SHA256).Hash.ToLower()) -eq ($($downloadNameResponse.downloads.application.sha256)))) {
            Write-Output "File Hash does not Match!"
            Write-Output "Downloading Latest Proxy Jar"
            Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/velocity/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/proxy.jar"
        }
    }
}

function Get-PluginHive() {

    if (-Not (Test-Path -Path "./src/plugins")) {
        New-Item -Name "./src/plugins" -ItemType "Directory" | Out-Null
    }

    Invoke-WebRequest -Uri "https://gitlab.com/binary-gaming-network/minecraft-hive-server/-/jobs/artifacts/master/raw/target/Hive-1.0-SNAPSHOT.jar?job=build" -Outfile "./src/plugins/hive.jar"
}

function New-DockerBuild() {
    if (!(Get-ChildItem -Path Env:\GITLAB_CI -ErrorAction SilentlyContinue)) {
        Start-Process -FilePath "docker" -ArgumentList "build . --tag bgn/velocity-proxy:latest" -NoNewWindow -Wait
    }
    else {
        Write-Output "Gitlab CI detected, not running docker build."
    }
}

Get-LatestProxyVersion
Get-PluginHive
New-DockerBuild