# Velocity Docker Container

This repo containers the process for making a Velocity Powered Minecraft Proxy.

This is not meant to be a general purpose Velocity Container, this is specifically used for the Hive and Hydra plugins for Velocity to automatically join and remove servers from the Cluster.

The main use case of this setup is to run Minecraft on Kubernetes without needing to manually add servers in the config file.

The following ENV's need to be set to use this:

* MOTD - This is optional
* PLAYERINFO - This is required so that Velocity knows what forwarding method to use, valid options are "Modern" or "None"
* SECRET - Required, needed for PLAYERINFO when "Modern" forwarding is used.

Example usage:

```docker run --rm -it -e MOTD="Welcome to Binary Gaming!" -e PLAYERINFO=Modern -e SECRET=1234567890 bgn/velocity-proxy:latest```