#!/bin/bash

[[ ! -z "$MOTD" ]] && MOTD="$MOTD" || MOTD="&#09add3A Velocity Server"
[[ ! -z "$PLAYERINFO" ]] && PLAYERINFO="$PLAYERINFO" || PLAYERINFO="modern"
[[ ! -z "$SECRET" ]] && echo "$SECRET" > forwarding.secret || { echo "ERROR: Please provide a Secret" ; exit 1; }
[[ ! -z "$ICONURL" ]] && curl "$ICONURL" -o "./server-icon.png"

echo "Generating Config"

cat << EOF > velocity.toml
motd = "$MOTD"
player-info-forwarding-mode = "$PLAYERINFO"
forwarding-secret-file = "forwarding.secret"
bind = "0.0.0.0:25565"
show-max-players = 500

[servers]
lobby = "127.0.0.1:30066"

try = [
  "lobby"
]

[forced-hosts]
"lobby.example.com" = [
  "lobby"
]

[advanced]
tcp-fast-open = true

[query]
map = "Velocity"
EOF

java -jar ./proxy.jar