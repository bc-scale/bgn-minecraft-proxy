FROM openjdk:latest

COPY ./src/ ./app/
EXPOSE 25565/tcp 25575/tcp 25599/tcp
WORKDIR /app

CMD [ "/bin/bash", "./run.sh" ]